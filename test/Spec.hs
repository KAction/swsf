{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE RecordWildCards    #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# OPTIONS_GHC -Wno-orphans #-}
import           Control.Lens
import           Data.Maybe
import qualified SWSF.Extra.Hex            as Hex
import qualified SWSF.Extra.Point          as Point
import           SWSF.Types
import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck.Arbitrary

deriving instance Show Point
deriving instance Show Coords

newtype Positive = Positive Int deriving Show
instance Arbitrary Positive where
  arbitrary = do
    n <- arbitrarySizedNatural
    pure $ Positive (n + 1)

instance Arbitrary Coords where
  arbitrary = do
    row' <- arbitrarySizedNatural
    col' <- arbitrarySizedNatural
    pure $ Coords row' col'
instance Arbitrary Point where
  arbitrary = do
    x' <- arbitrary
    y' <- arbitrary
    pure $ Point x' y'

data Offset = Offset { rowOff :: Int, colOff :: Int } deriving (Show, Eq)
instance Arbitrary Offset where
  arbitrary = do
    rowOff' <- arbitrarySizedNatural
    colOff' <- arbitrarySizedNatural
    pure $ if (rowOff', colOff') == (0, 0)
      then Offset 0 1
      else Offset rowOff' colOff'

errorS :: (Show a) => a -> b
errorS = error . show

-- This is size of hex .png file in resources/.  Not sure, that things
-- would not change, but this size is reasonable to test functions
-- in SWSF.Extra.Hex nevertheless.
hexsize :: Size
hexsize = _Size # (57 :: Int, 69 :: Int)

-- propery: If point is center of hex, Hex.locate returns coordinates of this hex.
prop_locateCenter :: Coords -> Bool
prop_locateCenter c = Just c == Hex.locate hexsize (Hex.center hexsize c)

-- property: No different hexes share same center
prop_uniqCenter :: Coords -> Offset -> Bool
prop_uniqCenter c (Offset {..}) = Hex.center hexsize c /= Hex.center hexsize c' where
  c' = c & row+~rowOff & col+~colOff


prop_walk_distance :: Point -> Positive -> Point -> Bool
prop_walk_distance dest_ (Positive n) cur =
  case Point.walk dest_ n cur of
    Nothing -> True
    Just next -> if Point.distance dest_ next < Point.distance dest_ cur
                 then True
                 else errorS next

prop_walk_nothing :: Point -> Positive -> Bool
prop_walk_nothing p (Positive n) = isNothing (Point.walk p n p)

main :: IO ()
main = hspec $ do
  --- XXX: Write meaningful testsuite
  describe "abs" $ do
    it "returns a positive number when given a negative number" $
      abs (-1) == (1 :: Int)
  describe "Hex" $ do
    prop "locate center" prop_locateCenter
    prop "unique center" prop_uniqCenter
  describe "Point" $ do
    prop "walk reduces distance" prop_walk_distance
    prop "Nothing when dest == cur" prop_walk_nothing
