{-# LANGUAGE LambdaCase      #-}
{-# LANGUAGE RankNTypes      #-}
{-# LANGUAGE TemplateHaskell #-}
----------------------------------------------------------------------------
-- |
-- | Module        : SWSF.Init
-- | Description   : initial state of game
-- | Copyright     : Dmitry Bogatov, 2017
-- | License       : GPL-3+
-- | Maintainer    : KAction@gnu.org
-- | Stability     : experimental
-- | Portability   : POSIX
-- |
----------------------------------------------------------------------------

module SWSF.Init (runW) where
import           Control.Lens.Extended
import           Control.Monad
import           Control.Monad.Catch
import           Control.Monad.IO.Class
import           Control.Monad.Loops
import           Control.Monad.Reader      (ReaderT, runReaderT)
import           Control.Monad.State       (evalStateT)
import qualified Data.Cache                as Cache
import           Data.List.NonEmpty        (NonEmpty)
import qualified Data.List.NonEmpty        as NE
import           Data.List.PointedList
import qualified Data.Vector               as V
import qualified Data.Yaml                 as Y
import qualified Graphics.UI.SDL           as S
import qualified Graphics.UI.SDL.Framerate as FPS
import qualified Graphics.UI.SDL.TTF       as Font
import           Prelude                   hiding (mod)
import           Stage0
import qualified SWSF.Extra.Hex            as Hex
import           SWSF.IO
import           SWSF.Types
import           SWSF.Types.Basic
import qualified SWSF.UI.Sprite            as Sprite
import           System.Directory
import           Text.Printf

{-
 For now among configuration loading routines only related
 to 'Background' extra information: screen resolution, but
 for consistency we define 'LoadConf' and put all loading
 routines into ReaderT monad.
-}

type LoadConf = Size
type Load a = ReaderT LoadConf IO a
type LoadName a = String -> Load a

loadFrame :: (MonadIO m) => FilePath -> m Frame
loadFrame path = do
  forward_  <- Sprite.load path
  backward_ <- Sprite.flop forward_
  pure $ Frame forward_ backward_

loadFrames :: (MonadIO m ) => FilePath -> String -> m (NonEmpty Frame)
loadFrames dir prefix_ = do
  let filename :: Int -> FilePath
      filename = printf "%s/%s-%d.png" dir prefix_
  images <- takeWhileM (liftIO . doesFileExist) (map filename [0..])
  NE.fromList <$> mapM loadFrame images

loadUnitLook :: LoadName UnitLook
loadUnitLook name = do
  let directory = printf "resources/units/%s" name :: String
      stillF    = printf "%s/still.png" directory
  stillS <- loadFrame stillF
  moves_ <- loadFrames directory "move"
  melee_ <- loadFrames directory "attack-melee"
  pure $ UnitLook stillS moves_ melee_

loadUnitStats :: LoadName UnitStats
loadUnitStats name = liftIO $ do
  let filepath = printf "resources/units/%s/stats.yaml" name
  Y.decodeFileEither filepath >>= \case
    Left e  -> throwM e
    Right s -> pure s

loadUnit :: LoadName Unit
loadUnit name = Unit <$> loadUnitStats name <*> loadUnitLook name

loadHex :: LoadName Hex
loadHex name =
  let path = printf "resources/hex/%s.png" name in
    Hex <$> Sprite.load path

loadBackground :: LoadName Background
loadBackground name = do
  let path = printf "resources/background/%s.png" name
  resolution <- view id
  Background <$> (Sprite.load path >>= Sprite.zoomTo resolution)

setupFPS :: Load (IO ())
setupFPS = liftIO $ do
  fps <- FPS.new
  void $ FPS.set fps 36
  pure $ FPS.delay fps

mkRenderFn :: S.Surface -> RenderFn
mkRenderFn s = RenderFn $ \act -> do
  let pf = S.surfaceGetPixelFormat s
  black <- liftIO $ S.mapRGB pf 0 0 0
  void . liftIO $ S.fillRect s Nothing black
  act (Sprite.blit s)
  liftIO $ S.flip s

-- Haskell bindings do not provide symbolic constants. These
-- values are copy-pasted from SDL C header.
enableKeyRepeat :: IO ()
enableKeyRepeat = void $ S.enableKeyRepeat 500 30

withFullscreen :: (S.Surface -> IO a) -> IO a
withFullscreen f = S.withInit [S.InitVideo, S.InitTimer] $ do
  True <- Font.init -- FIXME: Font.quit causes SIGSEGV
  let flags = [S.HWSurface, S.Fullscreen]
  S.Modes (rect:_) <- S.listModes Nothing flags
  let w = S.rectW rect
      h = S.rectH rect
  S.setVideoMode w h 32 flags >>= f

runW :: W () -> IO ()
runW w = withFullscreen $ \screen' -> do
  let resolution = _Size # (S.surfaceGetWidth screen',
                            S.surfaceGetHeight screen')
  enableKeyRepeat
  conf <- flip runReaderT resolution $ do
    unit_ <- $(loadRepo ''Unit) loadUnit
    hex_ <- $(loadRepo ''Hex) loadHex
    background_ <- $(loadRepo ''Background) loadBackground

    -- just as in original HOMM III
    gridsize_ <- pure $ Coords 10 14
    delay_    <- setupFPS
    pure $ WConf
      unit_
      hex_
      background_
      (GridInfo gridsize_ (Point 200 200))
      delay_
      (mkRenderFn screen')

  (u:us) <- Y.decodeFileEither "units.yaml" >>= \case
    Left err -> throwM err -- fatal
    Right (Y.Array arr) ->
      let get1unit = flip runReaderT conf . getBattleUnit
      in V.toList <$> traverse get1unit arr
    _ -> fail "units.yaml does not contain array"
  let units_ = PointedList [] u us
      wcache = WCache (Cache.new Hex.accessibleW)

  let state_ = WState units_ Nothing wcache False
  evalStateT (runReaderT w conf) state_
