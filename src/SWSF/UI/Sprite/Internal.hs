{-# LANGUAGE TemplateHaskell #-}
{-|
Module        : SWSF.Sprite
Description   : sprite abstraction
Copyright     : Dmitry Bogatov, 2017
License       : GPL-3+
Maintainer    : KAction@gnu.org
Stability     : experimental
Portability   : POSIX

This module defines abstract datatype Sprite, which is some
rectangular, possibly transparent image, that can be drawn on screen.

-}
module SWSF.UI.Sprite.Internal where
import           Control.Lens
import           Control.Monad.IO.Class
import           Data.Functor
import           Foreign                    (Ptr, withForeignPtr)
import qualified Graphics.UI.SDL            as S
import qualified Graphics.UI.SDL.Image      as SI
import qualified Graphics.UI.SDL.Rotozoomer as GFX
import           SWSF.Types.Basic

data Sprite = Sprite { _surface :: S.Surface, _ssize :: Size } deriving Eq
makeLenses ''Sprite

size :: Sprite -> Size
size = _ssize

makeSprite :: S.Surface -> Sprite
makeSprite s = let w = S.surfaceGetWidth s
                   h = S.surfaceGetHeight s in
                 Sprite s (Size w h)

load :: (MonadIO m) => FilePath -> m Sprite
load path = liftIO $ do
  temp <- SI.load path
  surface_ <- S.displayFormatAlpha temp
  S.freeSurface temp
  pure $ makeSprite surface_

blit :: (MonadIO m) => S.Surface -> Sprite -> Point -> m ()
blit screen s point = liftIO $ do
  let rect = S.Rect (view x point) (view y point) 0 0
  void $ S.blitSurface (view surface s) Nothing screen (Just rect)

{- Annoying case when type system gets in the way. -}
infixr 2 #/
(#/) :: Int -> Int -> Double
u #/ v = fromIntegral u / fromIntegral v

zoomTo :: (MonadIO m) => Size -> Sprite -> m Sprite
zoomTo wish s = liftIO $ do
  let factorX = view width wish  #/ view (ssize.width) s
      factorY = view height wish #/ view (ssize.height) s
  makeSprite <$> GFX.zoom (view surface s) factorX factorY True

foreign import ccall safe "surface_flop"
    cflop :: Ptr S.SurfaceStruct -> IO (Ptr S.SurfaceStruct)

flop :: (MonadIO m) => Sprite -> m Sprite
flop src = liftIO $ do
  withForeignPtr (view surface src) $
    \ptr -> makeSprite <$> (S.mkFinalizedSurface =<< cflop ptr)
