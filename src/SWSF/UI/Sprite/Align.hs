{-# LANGUAGE PatternSynonyms #-}
module           SWSF.UI.Sprite.Align (anchor, pattern UNIT, pattern CENTER) where
import           Control.Lens
import           SWSF.Types.Basic
import           SWSF.UI.Sprite   (Sprite, size)

----------------------------------------------------------------------------
-- | Blit unit sprite in a way, that its foots would be located on
-- | given point. Sprites are prepared in a way, that foots (or what
-- | ghost may have instead) are located at 1/2 of sprite horizonally and
-- | 3/4 from top vertically.
----------------------------------------------------------------------------
pattern UNIT :: (Double, Double)
pattern UNIT = (0.5, 0.75)

pattern CENTER :: (Double, Double)
pattern CENTER = (0.5, 0.5)

anchor :: (Double, Double) -> (Sprite -> Point -> a) -> Sprite -> Point -> a
anchor (xf, yf) put s p =
  let xoff = round $ xf * fromIntegral (s^.to size.width)
      yoff = round $ yf * fromIntegral (s^.to size.height)
  in put s (p & x-~ xoff & y -~ yoff)
