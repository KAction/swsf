{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell    #-}
module SWSF.UI.Font.Internal where
import           Control.Lens.Extended
import           Control.Monad.IO.Class
import           Control.Monad.State
import           Control.Once
import           Data.Hashable
import           GHC.Generics
import qualified Graphics.UI.SDL         as S
import           Graphics.UI.SDL.TTF     (Font)
import qualified Graphics.UI.SDL.TTF     as Font
import           Orphans                 ()
import           SWSF.UI.Sprite.Internal hiding (size)
import           System.IO.Unsafe

data FontStyle = Solid | Shaded S.Color | Blended
  deriving (Eq, Generic, Hashable)

data RenderOpts = RenderOpts {
  _style :: FontStyle,
  _size  :: Int,
  _color :: S.Color
  } deriving (Eq, Generic, Hashable)
makeLenses ''RenderOpts

fontpath :: String
fontpath = "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf"

defRenderOpts :: RenderOpts
defRenderOpts = RenderOpts Solid 24 (S.Color 0 0 0)

{-# NOINLINE cachedOpenFont #-}
cachedOpenFont :: Int -> IO Font
cachedOpenFont = unsafePerformIO $ once (Font.openFont fontpath)

{-# NOINLINE cachedRenderWith #-}
cachedRenderWith :: Font -> String -> RenderOpts -> IO Sprite
cachedRenderWith = unsafePerformIO $ once $ \font str opts -> do
  let fg = view color opts
  surface_ <- case view style opts of
    Solid     -> Font.renderUTF8Solid font str fg
    Shaded bg -> Font.renderUTF8Shaded font str fg bg
    Blended   -> Font.renderUTF8Blended font str fg
  pure $ makeSprite surface_

render :: MonadIO m => String -> State RenderOpts () -> m Sprite
render str mods = liftIO $ do
  let opts = execState mods defRenderOpts
  font <- cachedOpenFont (view size opts)
  cachedRenderWith font str opts
