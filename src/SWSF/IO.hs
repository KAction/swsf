{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
module SWSF.IO where
import Control.Lens
import           Control.Monad.Catch
import           Control.Monad.Reader.Class
import           Data.Yaml
import           SWSF.Types

data MarshalError = MarshalError String deriving Show
instance Exception MarshalError

getBattleUnit :: (MonadReader WConf m, MonadThrow m) => Value -> m BattleUnit
getBattleUnit (Object obj) = do
  archangel_ <- view $ unit.archangel
  dwarf_     <- view $ unit.dwarf
  skeleton_  <- view $ unit.skeleton
  let result = flip parseEither () $ \() -> do
        state_  <- obj .: "state"
        side_   <- obj .: "side"
        number_ <- obj .: "number"
        basen_  <- obj .: "base"
        baseu_  <- case basen_ of
          "archangel" -> pure archangel_
          "dwarf"     -> pure dwarf_
          "skeleton"  -> pure skeleton_
          _           -> fail $ "unknown base unit :" ++ basen_
        pure $ BattleUnit baseu_ (Right state_) side_ number_
  case result of
    Left err -> throwM $ MarshalError err
    Right x_ -> pure x_
getBattleUnit other_ = throwM $ MarshalError (show other_)
