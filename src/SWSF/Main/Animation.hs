module SWSF.Main.Animation (invoke) where
import           Control.Lens.Extended
import           Data.Cache
import           Data.List.PointedList.Circular
import qualified SWSF.Extra.Hex                 as Hex
import qualified SWSF.Extra.Point               as Point
import           SWSF.Types

invoke :: W ()
invoke = with (units.focus.state._Left) $ \mp -> do
  let dest_ = mp^.dest
  destp_ <- Hex.centerW dest_
  case Point.walk destp_ (mp^.speed) (mp^.current) of
    Nothing   -> do
      units.focus.state .= Right dest_
      units %= next -- move focus to next unit
      invalidate $ cache.accessible
    Just next_ -> units.focus.state .=
      Left (mp & current .~ next_ & frames %~ next)
