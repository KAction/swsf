module Data.Aeson.TH.Extended (
  module Data.Aeson.TH,
  mkJSON) where
import           Data.Aeson.TH
import           Data.Char
import           Language.Haskell.TH.Syntax

mkJSON :: Name -> Q [Dec]
mkJSON = deriveJSON opts where
  opts = defaultOptions {
    constructorTagModifier = map toLower,
        fieldLabelModifier = tail
    }
