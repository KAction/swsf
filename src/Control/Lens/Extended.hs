{-# LANGUAGE Rank2Types   #-}
{-# LANGUAGE ViewPatterns #-}
module Control.Lens.Extended (
  module Lens,
  declareUnderscoreFields,
  makeUnderscoreFields,
  with,
  ensure
  ) where
import           Control.Lens               as Lens
import           Control.Monad.State
import           Data.Char
import           Data.Monoid
import           Language.Haskell.TH.Lib
import           Language.Haskell.TH.Syntax

myRules :: LensRules
myRules = defaultFieldRules & lensField .~ classyUnderscoreNamer where
  classyUnderscoreNamer _ _ (nameBase -> field) = case field of
    '_' : x : xs ->
      let clsName = mkName $ "Has" ++ (toUpper x:xs)
          methName = mkName $ x:xs in
        [MethodName clsName methName]
    _            -> []


{-| Declare fields for every field with name starting with underscore.

Unlike default behaviour of 'declareFields', datatype name as prefix
is not expected, saving a lot of typing and helping to stay under 80
columns. -}
declareUnderscoreFields :: DecsQ -> DecsQ
declareUnderscoreFields = declareLensesWith myRules

makeUnderscoreFields :: Name -> DecsQ
makeUnderscoreFields = makeLensesWith myRules

with :: (MonadState s m, Monoid u)
     => Getting (First t) s t
     -> (t -> m u)
     -> m u
with p fn = do
  s <- use id
  case s^?p of
    Nothing -> pure mempty
    Just x  -> fn x

ensure :: (MonadState s m, Monoid u)
       => Getting (First t) s t
       -> m u
       -> m u
ensure p a = with p (const a)
