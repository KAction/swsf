{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE StandaloneDeriving #-}
module Orphans () where
import           Data.Hashable
import           Foreign.ForeignPtr
import           Foreign.ForeignPtr.Unsafe
import           Foreign.Ptr
import           GHC.Generics
import qualified Graphics.UI.SDL           as S

deriving instance Eq S.Color
deriving instance Generic S.Color
instance Hashable S.Color

ptrToInteger :: ForeignPtr a -> Integer
ptrToInteger = toInteger . ptrToIntPtr . unsafeForeignPtrToPtr

instance Hashable (ForeignPtr a) where
  hashWithSalt s p = hashWithSalt s (ptrToInteger p)
